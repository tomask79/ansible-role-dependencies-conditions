# Ansible Role Dependencies and Conditional Tasks executions #

Let's play with [Ansible](www.ansible.com) a little bit more. 
When deploying database or application server you often want to start tasks based 
on some conditions like...it doesn't make sense to deploy application server if database
isn't ready and so on. For this purpose you can use [Role Dependencies](http://docs.ansible.com/ansible/latest/playbooks_reuse_roles.html#role-dependencies)

Let's test it on easy task. 

**Task**: We're going to take [previous ansible sample](https://bitbucket.org/tomask79/ansible-roles-variables) 
with creating two text files. And let's add **new dependent role with name create_dir** which will create target
directory for the files produced by **create_file** role. And of course we can't create files if target directory 
isn't ready so we'll test conditional run of ansible task.

## create_dir role ##

First, we need to create create_dir role. We're going to need **just tasks** directory.

**...roles/create_dir/tasks/main.yml**

    - name: Checks whether directory exists
      shell: ls | grep outputdir
      ignore_errors: true
      register: output_exists

    - name: Creates directory
      file: path={{playbook_dir}}/outputdir state=directory
      when: output_exists.rc > 0

**The idea:**

First task registers "ls | grep outputdir" command running through the [shell](http://docs.ansible.com/ansible/latest/shell_module.html)
into the [variable](http://docs.ansible.com/ansible/latest/playbooks_variables.html#registered-variables) **output_exists**.
Now the trict is:

* when outputdir doesn't exists grep ends with exit code = 1
* when outputdir exists then grep ends with exit code = 0

when grep ends with result code 1 then ansible playbook execution ends, but we want to continue. 
Hence we also need **ignore_errors: true**

Now it makes sense to continue with second task only if output_exists.rc > 0 thus
we need **when: output_exists.rc > 0**
And that's it..:)

## Set dependency to create_dir role ##

We need to **run the create_dir role before the create_file role**.  
Let's add the dependency:

**...roles/create_file/meta/main.yml**

    dependencies:
      - { role: create_dir }

## Testing the playbook ##

On the first run: **ansible-playbook startPlaybook.xml**

we will see:

    PLAY [localhost] ************************************************************************************************************

    TASK [create_dir : Checks whether directory exists] *************************************************************************
    fatal: [localhost]: FAILED! => {"changed": true, "cmd": "ls | grep outputdir", "delta": "0:00:00.010885", "end": "2018-02-1012:08:39.651527", "msg": "non-zero return code", "rc": 1, "start": "2018-02-10 12:08:39.640642", "stderr": "", "stderr_lines": [], "stdout": "", "stdout_lines": []}
    ...ignoring

    TASK [create_dir : Creates directory] ***************************************************************************************
    changed: [localhost]

    TASK [create_file : This is play1 example] **********************************************************************************
    changed: [localhost]

    TASK [create_file : This is play2 example] **********************************************************************************
    changed: [localhost]

    PLAY RECAP ******************************************************************************************************************
    localhost 

as you can see "Creates directory" was launched, because outputdir directory didn't exists. 
Now let's run the playbook again. And the output should be:

    PLAY [localhost] ************************************************************************************************************

    TASK [create_dir : Checks whether directory exists] *************************************************************************
    changed: [localhost]

    TASK [create_dir : Creates directory] ***************************************************************************************
    skipping: [localhost]

    TASK [create_file : This is play1 example] **********************************************************************************
    ok: [localhost]

    TASK [create_file : This is play2 example] **********************************************************************************
    ok: [localhost]

    PLAY RECAP ******************************************************************************************************************
    localhost 

"Creates directory" task was skipped, because outputdir directory was already created by previous run.
And that's what we wanted...:-)

regards

Tomas